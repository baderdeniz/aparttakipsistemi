﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apart_Takip_Sistemi.Models.ORM
{
    public class LoginModel
    {
        public string KullaniciAdi { get; set; }
        public string Sifre { get; set; }
    }
}