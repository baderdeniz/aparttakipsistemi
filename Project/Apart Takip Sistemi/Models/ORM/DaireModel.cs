﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apart_Takip_Sistemi.Models.ORM
{
    public class DaireModel
    {
        public string Blok { get; set; }

        [Required]
        public Nullable<int> DaireNo { get; set; }
    }
}