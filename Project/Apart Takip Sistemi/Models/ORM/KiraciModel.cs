﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apart_Takip_Sistemi.Models.ORM
{
    public class KiraciModel
    {
        public int KiraciID { get; set; }
        public Nullable<int> DaireID { get; set; }
        public string KullaniciAdi { get; set; }
        public string Sifre { get; set; }
        [Required]
        public string Adi { get; set; }
        [Required]
        public string Soyadi { get; set; }
        [Required]
        [StringLength(11)]
        public string Tc { get; set; }
        [Required]
        [StringLength(11)]
        public string Telefon { get; set; }
    }
}