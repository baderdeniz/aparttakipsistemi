//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Apart_Takip_Sistemi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class daireler
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public daireler()
        {
            this.kiracilar = new HashSet<kiracilar>();
            this.kiralar = new HashSet<kiralar>();
        }
    
        public int DaireID { get; set; }
        public Nullable<int> DaireNo { get; set; }
        public string Blok { get; set; }
        public string DoluMu { get; set; }
        public string Kira { get; set; }
        public Nullable<int> GecikmeVarMi { get; set; }
        public Nullable<int> SonOdemeGunu { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<kiracilar> kiracilar { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<kiralar> kiralar { get; set; }
    }
}
