﻿using Apart_Takip_Sistemi.Models;
using Apart_Takip_Sistemi.Models.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace Apart_Takip_Sistemi.Controllers
{
    public class LoginController : Controller
    {
        private apartEntities db = new apartEntities();

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginModel user)
        {
            if(ModelState.IsValid)
            {
                kiracilar kiraci = db.kiracilar.FirstOrDefault(k => k.KullaniciAdi == user.KullaniciAdi && k.Sifre == user.Sifre);
                
                if(kiraci != null)
                {
                    Session.Add("name", kiraci.Adi);
                    Session.Add("surname", kiraci.Soyadi);
                    Session.Add("kiraci", true);
                    return RedirectToAction("Index", "Kiraci");
                }

                admin a = db.admin.FirstOrDefault(k => k.KullaniciAdi == user.KullaniciAdi && k.Sifre == user.Sifre);

                if (a != null)
                {
                    Session.Add("name", a.Adi);
                    Session.Add("surname", a.Soyadi);
                    Session.Add("admin", true);
                    return RedirectToAction("Index", "Admin");
                }
            }
            return View();
        }
    }
}