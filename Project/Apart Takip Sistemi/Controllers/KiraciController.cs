﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Apart_Takip_Sistemi.Controllers
{
    
    public class KiraciController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            if (Session["kiraci"] == null)
                return RedirectToAction("Index", "Login");
            return View();
        }

        public ActionResult Cikis()
        {
            //Session.Remove("loginUser");
            Session.Clear();
            return Redirect("/Login");
        }
    }
}