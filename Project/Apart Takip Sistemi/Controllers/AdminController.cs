﻿using Apart_Takip_Sistemi.Models;
using Apart_Takip_Sistemi.Models.ORM;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Apart_Takip_Sistemi.Controllers
{
    public class AdminController : Controller
    {
        private apartEntities db = new apartEntities();

        // GET: Admin
        public ActionResult Index()
        {
            if (Session["admin"] == null)
                return RedirectToAction("Index", "Login");
            return View(db.daireler.ToList());
        }

        public ActionResult Kiralar()
        {
            if (Session["admin"] == null)
                return RedirectToAction("Index", "Login");

            return View(db.daireler.Where(w => w.DoluMu == "dolu").ToList());
        }

        public ActionResult DaireBilgi(int id)
        {
            if (Session["admin"] == null)
                return RedirectToAction("Index", "Login");
            ViewBag.daire = db.daireler.Find(id);
            List<KiraciModel> k = db.kiracilar.Where(w=>w.DaireID==id).Select(s=>new KiraciModel
            {
                KiraciID=s.KiraciID,
                DaireID=s.DaireID,
                Adi=s.Adi,
                Soyadi=s.Soyadi
                                
            }).ToList();
            return View(k);
        }

        [HttpPost]
        public ActionResult DaireBilgi(KiraciModel kiraci)
        {
            if (ModelState.IsValid)
            {
                kiracilar k = new kiracilar();
                k.Adi = kiraci.Adi;
                k.Soyadi = kiraci.Soyadi;
                k.DaireID = kiraci.DaireID;
                k.Tc = kiraci.Tc;
                k.Telefon = kiraci.Telefon;
                k.KullaniciAdi = "k";
                k.Sifre = "123";
                db.kiracilar.Add(k);
                db.SaveChanges();
                

            }
            return RedirectToAction("DaireBilgi/"+kiraci.DaireID,"Admin");
        }

        public ActionResult KiraciDuzenle(int id)
        {
            if (Session["admin"] == null)
                return RedirectToAction("Index", "Login");
            kiracilar k = db.kiracilar.Find(id);
            KiraciModel kiraci = new KiraciModel();

            kiraci.Adi = k.Adi;
            kiraci.Soyadi = k.Soyadi;
            kiraci.Tc = k.Tc;
            kiraci.Telefon = k.Telefon;
            ViewBag.daire = db.daireler.Find(k.DaireID);
            ViewBag.kiraci = db.kiracilar.Find(id);

            return View();
        }

        [HttpPost]
        public ActionResult KiraciDuzenle(kiracilar kiraci)
        {
            if(ModelState.IsValid)
            {
                
                db.Entry(kiraci).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("DaireBilgi/" + kiraci.DaireID, "Admin");
            }
            ViewBag.kiraci = db.kiracilar.Find(kiraci.KiraciID);
            ViewBag.daire = db.daireler.Find(kiraci.DaireID);
            return View(kiraci);
            
        }       

        public ActionResult DaireEkle()
        {
            if (Session["admin"] == null)
                return RedirectToAction("Index", "Login");
            return View();
        }

        [HttpPost]
        public ActionResult DaireEkle(DaireModel daire)
        {
            if (ModelState.IsValid)
            {
                daireler d = new daireler();
                d.Blok = daire.Blok;
                d.DaireNo = daire.DaireNo;
                d.DoluMu = "bos";
                d.GecikmeVarMi = 0;
               
                db.daireler.Add(d);
                db.SaveChanges();
                ViewBag.eklendi = "1";
                return View();
                
            }
            ViewBag.eklendi = "0";
            return View();
        }

        [HttpPost]
        public ActionResult KiraciEkle(KiraciModel kiraci)
        {
            if (ModelState.IsValid)
            {
                kiracilar k = new kiracilar();
                k.Adi = kiraci.Adi;
                k.Soyadi = kiraci.Soyadi;
                db.kiracilar.Add(k);
                db.SaveChanges();
                return View();

            }
            return View();
        }


        [HttpPost]
        public void daireSil(int id)
        {
            try
            {
                var daire = db.daireler.FirstOrDefault(d => d.DaireID == id);
                db.daireler.Remove(daire);
                db.SaveChanges();
                Response.Write("1");
                
            }
            catch
            {
                Response.Write("0");
            }
        }

        [HttpPost]
        public void KisiSil(int id)
        {
            try
            {
                var kisi = db.kiracilar.FirstOrDefault(d => d.KiraciID == id);
                db.kiracilar.Remove(kisi);
                db.SaveChanges();
                Response.Write("1");

            }
            catch
            {
                Response.Write("0");
            }
        }

        public ActionResult Cikis()
        {
            Session.Clear();
            return Redirect("/Login");
        }
    }
}